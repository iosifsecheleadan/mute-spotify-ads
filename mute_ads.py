# by iosifsecheleadan

from os import popen
from time import sleep

from dbus_api import DbusAPI


def get_spotify_sink_id():
	""" Return Spotify's Sink Id
	If Spotify isn't running, nothing is returned 
	"""

	# get all sink input ids and lines containing spotify
	# Spotify's sink id should be on the line
	# before the first line containing "spotify"
	result = popen("pactl list sink-inputs | grep -E \"Sink Input #|spotify\"").readlines()

	for i in range(len(result)):
		# find the first line containing spotify
		if "spotify" in result[i]:
			# the previous line holds spotify's sink id, formatted like
			# Sink Input #[id]
			string = result[i-1]
			return string[string.rfind("#")+1 : -1]
			

def result_to_str(result):
	"""
	Results are formatted like b'[song-title]' by default
	This function de-formats them into just [song-title]
	"""
	return str(result)[2 : len(str(result))-1]


class Listener:
	def __init__(self):
		""" Load the Dbus API """
		self.dbus = DbusAPI()
	
	def get_current_track(self):
		""" Return Currently playing Spotify track name """
		try: return result_to_str(self.dbus.get_spotify_now_playing().lines[1])
		except Exception as e: return "Unknown Track"

	def run(self):
		"""
		Check the currently playing song every second
		If it is a different song, 
			And it is an advert: mute Spotify
			Otherwise: unmute Spotify
		Repeat indefinitely
		"""
		previous_track = ""
		while True:
			current_track = self.get_current_track()
			spotify_sink_id = get_spotify_sink_id()

			if current_track != previous_track:
				if current_track == "Advertisement":
					popen("pactl set-sink-input-volume " + str(spotify_sink_id) + " 0%")
					print("Muted Advertisement")
				else: 
					popen("pactl set-sink-input-volume " + str(spotify_sink_id) + " 100%")
					print("Playing " + str(current_track))
			previous_track = current_track
			sleep(1)


Listener().run()

