# Mute Spotify Ads

This python script mutes Spotify while Advertisements are beind played.

This program uses the Dbus API from Thrasd's [spotify-now-playing-terminal](https://github.com/Thrasd/spotify-now-playing-terminal). 

## Usage

### Running the script 

1. Python

```sh
python mute_ads.py
```

2. Bash

A bash script has been created, that simply runs the python script. 
First, make the script executable, then run it.

```sh
chmod +x mute_ads.sh
./mute_ads.sh
```

### Run the application at startup

You can also make the application run at startup 
to avoid running it every time you open Spotify.

This is distro and desktop specific. 

## Troubleshooting

If ads aren't muted, try restarting Spotify. 

## Technical details

The script uses the above mentioned Dbus API to find out what Spotify is playing.

It then mutes or unmutes Spotify's volume using the commands: 

```sh
# mute
pactl set-sink-input-volume [spotify id] 0% 

# unmute
pactl set-sink-input-volume [spotify id] 100% 
```

*This operation is repeated every second, 
so sometimes you might hear a fraction of a second of an ad, 
or not hear a fraction of a second of a song. This is rarely noticeable.*


